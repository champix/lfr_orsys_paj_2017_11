function parseXML(val) {
    if (document.implementation && document.implementation.createDocument) {
        xmlDoc = new DOMParser().parseFromString(val, 'text/xml');
    } else if (window.ActiveXObject) {
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.loadXML(val);
    } else {
        alert('Your browser can\\'
            t handle this script ');
            return null;
        }
        return xmlDoc;
    }