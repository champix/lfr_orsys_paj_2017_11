function createXmlDoc() {
    var xml = document.implementation.createDocument('', '', null);
    var root = xml.createElement('root');
    xml.appendChild(root);
    root.appendChild(xml.createElement('child1'));

    console.log(xml);
}


'use strict';

function parseXML(val) {
    if (document.implementation && document.implementation.createDocument) {
        xmlDoc = new DOMParser().parseFromString(val, 'text/xml');
    } else if (window.ActiveXObject) {
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.loadXML(val);
    } else {
        alert('Your browser can\'t handle this script ');
        return null;
    }
    return xmlDoc;
}

function loadContact() {
    //1. recup du XHR
    var _xhr = new XMLHttpRequest();
    //2. open
    _xhr.open('GET', 'vues/contact.html');
    //3. set onreadystatechange function
    _xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {

            document.getElementById('main-wrapper').innerHTML = this.responseText;
            console.log(this.responseText);
        }
    };
    _xhr.send();
}

function loadData(resource) {
    if (resource === undefined) return null;

    var _xhr = new XMLHttpRequest();
    _xhr.open('GET', 'http://localhost:775/' + resource);
    _xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            console.log(this.responseText);
            console.log(resource);
        }
    };
    _xhr.send();
}






function initPage(event) {
    // event.stopPropagation()
    console.log('function init');
    console.log(event);
    var elem = document.querySelector('#navbar-a-contact');


    elem.addEventListener('click', function(event) {
        console.log(elem);
        return myAjax('vues/contact.html', 'main-wrapper');
    });

    elem = document.getElementById('input-find');
    elem.addEventListener('change', function(evt) {

        /*loadTemplate('vues/produits.xsl', 'main-wrapper', {
            dataUrl: 'http://MW44100/get.php?resource=produits'
          
        });*/
        document.getElementById('infobulle').style.display = 'block';
        return loadTemplate('vues/produitFind.xsl', 'infobulle', {
            dataUrl: 'http://MW44100/get.php?resource=produits',

        });

    });
    // elem.addEventListener('blur', function(evt) {
    //     document.getElementById('infobulle').style.display = 'none';

    // })


}
//window.addEventListener('load', initPage);
window.onload = initPage;
// myAjax('http.../','main-wrapper',{method:'GET',header:'application/json'})
function myAjax(templateUrl, wrapperId, options) {
    //if (undefined !== options) options = {};
    //etape 1 
    var _xhr = new XMLHttpRequest({ anon: true, system: true });
    //etape 2
    var method = (options !== undefined && options.method !== undefined && options.method.length >= 3 ? options.method : 'GET');
    _xhr.open(method, templateUrl, true)

    //ajout des headers 
    var headerAccept = (options !== undefined && options.header !== undefined && options.header.length >= 3 ? options.header : 'application/json,text/*,application/xhtml+xml,application/xml');
    _xhr.setRequestHeader('Accept', headerAccept);
    //etape 4
    _xhr.onreadystatechange = function() {


            if (_xhr.readyState == XMLHttpRequest.DONE && _xhr.status >= 200 && _xhr.status < 300) {
                console.log(this.getAllResponseHeaders());
                //JSON
                if (_xhr.getResponseHeader('content-type').includes('application/json')) {
                    console.log('j\'ai bien recu des datas');
                    options.data = JSON.parse(_xhr.responseText);
                    options.type = 'application/json';
                } else if (_xhr.getResponseHeader('content-type').includes('text/xml')) {
                    options.data = _xhr.responseXML;
                    options.type = 'text/xml';

                }
                //HTML & Others                
                else {
                    var element = document.getElementById(wrapperId);

                    if (options !== undefined && options.populate !== undefined) {
                        var templatePopulated = options.populate(_xhr);
                        console.log(templatePopulated);
                        element.innerHTML = templatePopulated;
                    } else {
                        element.innerHTML = _xhr.responseText;
                    }

                    var scripts = element.querySelectorAll('script');
                    scripts.forEach(function(script) {
                        var scriptFromDom = document.createElement('script');
                        if (script.src.length > 3) {
                            var att = document.createAttribute('src');
                            att.value = script.src;
                            scriptFromDom.attributes.setNamedItem(att);
                        } else {
                            scriptFromDom.appendChild(document.createTextNode(script.innerHTML));
                        }

                        document
                            .head
                            .append(scriptFromDom);
                    });
                    console.log(scripts);


                    console.log('default type recieved');

                }
                if (options !== undefined && options.callback !== undefined) options.callback();
            }
        }
        //etape
    _xhr.send();
}
/*params{
    dataUrl:'',
    prefix:'',
}
*/
/*loadTemplate('vues/contact.html', 'main-wrapper', {
    dataUrl: 'http://775/contact',
    prefix: 'contact'
});*/
/**
 * Fonction de remplissage des template par echapement sous la forme
 * /{{ prefix }}/
 * @param {*} templateUrl 
 * @param {*} wrapperId 
 * @param {*} params 
 */
function loadTemplate(templateUrl, wrapperId, params) {

    var options = {
        wrapperId: wrapperId,
        populate: undefined,
        callback: function() {
            //appel de la presentation html  ou xsl 
            return myAjax(templateUrl, wrapperId, {
                wrapperId: wrapperId,
                callback: undefined,
                populate: function(xhrResponse) {
                    return populateTemplate(xhrResponse.responseText, options.data, params.prefix, options.type);
                }
            })
        }
    }
    params !== undefined &&
        params.dataUrl !== undefined ?
        myAjax(params.dataUrl, null, options) : options.callback();

}



function populateTemplate(template, data, prefix, type) {

    if (type === 'text/xml') {
        var xsltProcessor = new XSLTProcessor();
        xsltProcessor.importStylesheet(parseXML(template));
        template = xsltProcessor.transformToFragment(data, document);
        template = new XMLSerializer().serializeToString(template);
        // var template = document.createTextNode(xmlText);

    } else if (type === 'application/json') {
        var datasName = Object.getOwnPropertyNames(data);
        // var t2=template
        datasName.forEach(function(oneName) {
            //creation de la chaine d'eval
            var strName = 'data.' + oneName;
            console.log(strName);
            //recuperation de la valeur evaluée
            var oneValue = eval(strName);
            console.log(oneValue);
            //soit cest un object
            if (typeof(oneValue) == 'object') {

                template = populateTemplate(template, oneValue, prefix + '-' + oneName, type);
            }
            //soit cest une value
            else {
                //creation de la chaine d'echapement dans le template
                var replacementCharSection = '/{{' + prefix + '-' + oneName + '}}/'
                console.log(replacementCharSection);
                //affichage avant changements
                console.log('template avant les replace');
                console.log(template);

                template = template.split(replacementCharSection).join(oneValue);

                //affichage une fois remplacé
                console.log('template avant les replace');
                console.log(template);
            }

        });
    }
    return template;
}