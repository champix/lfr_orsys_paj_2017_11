<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>
	<xsl:template match="/">
		<div class="produits">
			<xsl:for-each select="produits/produit">
				<xsl:apply-templates select="."/>
			</xsl:for-each>
		</div>
	</xsl:template>
	<xsl:template match="produit">
		<div class="produit">
			<table>
				<tr>
					<th>
						<img alt="{titre}" src="{src}" style="max-width:150px;max-height:150px;size:auto;"/>
					</th>
					<td>
						<xsl:value-of select="titre"/>
						<br/>
						<u>description :</u>
						<br/>
						<xsl:value-of select="description"/>
						<br/>
						<u>Prix :</u>
						<xsl:value-of select="prix"/>
					</td>
				</tr>
			</table>
		</div>
	</xsl:template>
</xsl:stylesheet>
