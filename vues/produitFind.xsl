<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:param select="'500'" name="findentry"></xsl:param>
	<xsl:output method="html" indent="no" omit-xml-declaration="yes"/>
	<xsl:template match="/">
		<div class="produits">
			<xsl:for-each select="produits/produit">
			<!-- [contains(./titre,$findentry)] -->
				<xsl:apply-templates select="."/>
			</xsl:for-each>
		</div>
	</xsl:template>
	<xsl:template match="produit">
		<div class="produit">
			<table>
				<tr>
					<th>
						<img alt="{titre}" src="{src}" style="width:50px;height:50px;size:auto;"/>
					</th>
					<td>
						<xsl:value-of select="titre"/>
						
					</td>
				</tr>
			</table>
		</div>
	</xsl:template>
</xsl:stylesheet>
